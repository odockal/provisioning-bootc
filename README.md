# provisioning-bootc

A bootc container for provisioning the AI workflows testing framework

## Building the image

```shell
make build
```
